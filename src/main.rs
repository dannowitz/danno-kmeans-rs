use rand::prelude::*;
use std::cmp::Ordering;
use std::cmp::PartialOrd;
use std::default::Default;

/// KMeansPP : The KMeans++ initialization, which considers data in the whole distribution when
///     selecting centroids
/// Random : Select random data points to serve as centroids
#[derive(Debug, Clone, Copy, Hash)]
enum KMeansInit {
    KMeansPP,
    Random,
}

/// Full : Classical EM KMeans algorithm.
/// Elkon : Elkon KMeans algorithm. Faster, but more memory usage.
#[derive(Debug, Clone, Copy, Hash)]
enum KMeansAlgorithm {
    Full,
    Elkan,
}

/// KMeans Clustering Structure
///
/// Parameters
/// ----------
/// init : KMeansInit
///     Algorithm by which cluster centroids are initialized.
///     KMeansPP option "intelligently" selects initial cluster
///     centroids to speed up convergence. Random selects them randomly.
/// algorithm : KMeansAlgorithm
///     Algorithm by which the clustering is performed.
///     Full is the basic EM step algorithm.
/// n_clusters : usize
///     Number of cluster centroids to initialize and clusters to render
/// tolerance : f64
///     If the difference in Frobenius norms between two iterations is less
///     than this, consider the solution converged.
/// n_init : usize
///     The number of times to run KMeans with different initializations
///     before deciding on cluster centroids and final clustering
/// R : Rng
///     Random seed to set for reproducibility
/// max_iter : usize
///     Maximum number of iterations to allow during fit
///
#[derive(Debug, Clone)]
struct KMeans {
    init: KMeansInit,
    algorithm: KMeansAlgorithm,
    n_clusters: usize,
    tolerance: f64,
    n_init: usize,
    cluster_centroids: Option<Vec<Vec<f64>>>,
    labels: Option<Vec<usize>>,
    inertia: Option<f64>,
    max_iter: usize,
}

/// Default values for a KMeans instantiation.
// QUESTION: How to make some values required at instantiation
//   and some values optional (set to default values)?
impl Default for KMeans {
    fn default() -> Self {
        KMeans {
            n_clusters: 2_usize,
            init: KMeansInit::Random,
            algorithm: KMeansAlgorithm::Full,
            tolerance: 1e-4_f64,
            n_init: 2,
            cluster_centroids: None,
            labels: None,
            inertia: None,
            max_iter: 300,
        }
    }
}

impl KMeans {
    fn new() -> KMeans {
        KMeans {
            ..Default::default()
        }
    }

    /// Perform the fitting process n_iter times and keep the best
    /// cluster centroids
    pub fn fit<R: Rng>(&mut self, x: &[Vec<f64>], rng: Option<&mut R>) {
        let n_samples: usize = x.len();
        if n_samples == 0 {
            println!("Empty data set supplied, aborting fit procedure.");
            return ();
        }
        if n_samples <= self.n_clusters {
            println!(
                "Not enough data to cluster. Only {} data points with {} clusters",
                n_samples, self.n_clusters
            );
            return ();
        }
        let mut best_labels: Option<Vec<usize>> = None;
        let mut best_inertia: Option<f64> = None;
        let mut best_centroids: Option<Vec<Vec<f64>>> = None;

        // Each iteration will need its own RNG
        let mut iter_rngs: Vec<SmallRng> = get_rngs(rng, self.n_init);

        for iter_rng in &mut iter_rngs {
            let (labels, inertia, centroids): (Vec<usize>, f64, Vec<Vec<f64>>) =
                self.kmeans_single(x, iter_rng);

            match best_inertia {
                None => {
                    best_labels = Some(labels);
                    best_inertia = Some(inertia);
                    best_centroids = Some(centroids);
                }
                Some(v) => {
                    if inertia < v {
                        best_labels = Some(labels);
                        best_inertia = Some(inertia);
                        best_centroids = Some(centroids);
                    }
                }
            }
        }
        self.labels = best_labels;
        self.inertia = best_inertia;
        self.cluster_centroids = best_centroids;
    }

    /// A single run of kmeans algorithm with its own initialization
    ///
    /// Parameters
    /// ----------
    /// x : Vec<Vec<f64>>
    ///     Data to fit to
    /// rng : Rng
    ///     Random generator that's used to initialize the cluster centroids
    ///
    /// Returns
    /// -------
    /// (labels, inertia, cluster centroids)
    ///
    fn kmeans_single<R: Rng>(
        &self,
        x: &[Vec<f64>],
        rng: &mut R,
    ) -> (Vec<usize>, f64, Vec<Vec<f64>>) {
        match self.algorithm {
            KMeansAlgorithm::Full => self.kmeans_single_full(x, rng),
            KMeansAlgorithm::Elkan => self.kmeans_single_elkon(x, rng),
        }
    }

    /// A single run of kmeans EM algorithm
    ///
    /// This algorithm
    ///   * Randomly select n_cluster data points to be the centroids
    ///   * For each data point, assign the label of the nearest centroid (E-step)
    ///   * Recalculate the mean of the centroids to be the mean of all of its data points (M-step)
    ///   * Repeat the E and M steps until (1) max_iter is hit, (2) the labels don't change,
    ///     or (3) change in inertia is below the assigned tolerance.
    ///
    /// Parameters
    /// ----------
    /// x : Vec<Vec<f64>>
    ///     Data to fit to
    /// seed : u64
    ///     Random seed that's used to initialize the cluster centroids
    ///
    /// Returns
    /// -------
    /// (labels, inertia, cluster centroids)
    ///
    fn kmeans_single_full<R: Rng>(
        &self,
        x: &[Vec<f64>],
        rng: &mut R,
    ) -> (Vec<usize>, f64, Vec<Vec<f64>>) {
        let n_samples: usize = x.len();

        let mut labels: Vec<usize> = vec![0_usize; n_samples];
        let mut inertia: f64 = std::f64::INFINITY;
        let mut centroids: Vec<Vec<f64>> = self.initialize_centroids(x, rng);

        // Do this until labels don't change or inertia change is below tolerance
        for i in 0..self.max_iter {
            // E-step. Assign each data point to its closest centroid's label
            let next_labels: Vec<usize> =
                x.iter().map(|y| closest_centroid(&y, &centroids)).collect();

            // M-step. Re-assign each center to be the mean of all constituent points
            // Count how many samples are in each cluster
            centroids = self.recalculate_centroids(&next_labels, x);

            // Calculate inertia
            let next_inertia = self.calculate_inertia(&next_labels, x, &centroids);

            // If labels did not change, then consider it converged and exit
            if labels.iter().zip(&next_labels).all(|(a, b)| *a == *b) {
                println!("Labels did not change, quitting algorithm.");
                break;
            } else {
                labels = next_labels.clone();
            }

            // If change in inertia is below tolerance, consider it converged
            if (inertia - next_inertia) < self.tolerance {
                println!("Inertial difference below specified tolerance, quitting algorithm.");
                break;
            } else {
                inertia = next_inertia
            };

            if i == self.max_iter - 1 {
                println!("Max iterations met, quitting algorithm.");
            }
        }

        (labels, inertia, centroids)
    }

    /// Calculate the inertia of the configuration
    ///
    /// Inertia here is defined as the sum of the square of the distances between
    /// each data point and its cluster centroid
    fn calculate_inertia(&self, labels: &[usize], x: &[Vec<f64>], centroids: &[Vec<f64>]) -> f64 {
        x.iter().enumerate().fold(0.0_f64, |mut acc, (i, data)| {
            acc += pairwise_distance(&data, &centroids[labels[i]]).powi(2);
            acc
        })
    }

    /// Given a set of points and their cluster labels, recalculate the centroid of
    /// each cluster.
    fn recalculate_centroids(&self, labels: &[usize], x: &[Vec<f64>]) -> Vec<Vec<f64>> {
        let n_features: usize = x[0].len();
        let mut cluster_sample_count: Vec<usize> = vec![0; self.n_clusters as usize];

        // Add up all the points for each cluster
        let centroids: Vec<Vec<f64>> = labels.iter().zip(x.iter()).fold(
            vec![vec![0.0_f64; n_features]; self.n_clusters as usize],
            |mut acc, (&label, point)| {
                acc[label] = vec_add(&acc[label], point);
                cluster_sample_count[label] += 1;
                acc
            },
        );

        // Normalize centroid sums by the number of points in each cluster
        (0..self.n_clusters as usize)
            .map(|i| {
                centroids[i]
                    .iter()
                    .map(|coord| coord / cluster_sample_count[i] as f64)
                    .collect()
            })
            .collect()
    }

    /// Initialize the cluster centroids
    fn initialize_centroids<R: Rng>(&self, x: &[Vec<f64>], rng: &mut R) -> Vec<Vec<f64>> {
        println!("Initializing cluster centroids with {:?}", self.init);
        match self.init {
            KMeansInit::Random => self.initialize_centroids_random(x, rng),
            KMeansInit::KMeansPP => self.initialize_centroids_kmeanspp(x, rng),
        }
    }

    /// Select N random samples to serve as the cluster centroids
    fn initialize_centroids_random<R: Rng>(&self, x: &[Vec<f64>], rng: &mut R) -> Vec<Vec<f64>> {
        let n_samples = x.len();
        // Randomly get the indexes of the data points
        let centroid_ixs: Vec<usize> = (0..self.n_clusters)
            .map(|_| rng.gen_range(0, n_samples))
            .collect();
        // Return the data points that correspond to these indexes
        centroid_ixs.iter().map(|y| x[*y].clone()).collect()
    }

    /// Assign data points to their nearest centroid
    pub fn predict(&self, x: &Vec<Vec<f64>>) -> Option<Vec<usize>> {
        match &self.cluster_centroids {
            None => {
                println!("KMeans has not yet fit to any data.");
                None
            }
            Some(centroids) => Some(x.iter().map(|y| closest_centroid(&y, &centroids)).collect()),
        }
    }

    /// PLACEHOLDER
    fn kmeans_single_elkon<R: Rng>(
        &self,
        x: &[Vec<f64>],
        rng: &mut R,
    ) -> (Vec<usize>, f64, Vec<Vec<f64>>) {
        (
            vec![1, 2, 3],
            10.0_f64,
            vec![vec![0.0, 1.1], vec![1.1, 2.2]],
        )
    }

    /// PLACEHOLDER
    fn initialize_centroids_kmeanspp<R: Rng>(&self, x: &[Vec<f64>], rng: &mut R) -> Vec<Vec<f64>> {
        vec![vec![1.0, 2.0]]
    }
}

/// Calculate the Euclidean pairwise distance for two Vec<f64>'s
pub fn pairwise_distance(x1: &[f64], x2: &[f64]) -> f64 {
    assert!(x1.len() == x2.len());
    (0..x1.len())
        .map(|i: usize| (x2[i] - x1[i]).powi(2))
        .sum::<f64>()
        .sqrt()
}

/// Perform addition on two float vectors
pub fn vec_add(v1: &Vec<f64>, v2: &Vec<f64>) -> Vec<f64> {
    assert!(v1.len() == v2.len());
    (0..v1.len()).fold(vec![0.0_f64; v1.len()], |mut acc, i| {
        acc[i] += v1[i] + v2[i];
        acc
    })
}

// Given a point and a list of centroids, return the index (label)
// of the closes centroid
pub fn closest_centroid(x: &[f64], centroids: &[Vec<f64>]) -> usize {
    let distances: Vec<f64> = centroids.iter().map(|c| pairwise_distance(c, x)).collect();
    argmin(&distances)[0]
}

/// Indices of the smallest element(s) in xs.
///
/// If there is more than one smallest element, `argmin` returns the indices of
/// all replicates.
///
/// # Examples
///
/// ```rust
/// let xs: Vec<u8> = vec![1, 2, 3, 4, 5, 4, 1];
/// let ys: Vec<u8> = vec![1, 2, 3, 4, 5, 4, 0];
///
/// assert_eq!(argmax(&xs), vec![0, 6]);
/// assert_eq!(argmax(&ys), vec![6]);
/// ```
pub fn argmin<T: PartialOrd>(xs: &[T]) -> Vec<usize> {
    if xs.is_empty() {
        vec![]
    } else if xs.len() == 1 {
        vec![0]
    } else {
        let mut minval = &xs[0];
        let mut min_ixs: Vec<usize> = vec![0];
        for (i, x) in xs.iter().enumerate().skip(1) {
            match x.partial_cmp(minval) {
                Some(Ordering::Less) => {
                    minval = x;
                    min_ixs = vec![i];
                }
                Some(Ordering::Equal) => min_ixs.push(i),
                _ => (),
            }
        }
        min_ixs
    }
}

/// Given a random seed, get a number of new random seeds to use
pub fn get_rngs<R: Rng>(rng: Option<&mut R>, n_seeds: usize) -> Vec<SmallRng> {
    match rng {
        // If no RNG is supplied, create one and use it to seed the SmallRngs
        None => {
            let mut seed_rng = rand::thread_rng();
            let seeds: Vec<u64> = (0..n_seeds)
                .map(|_| seed_rng.gen_range(0, 100_000))
                .collect();
            let rng_vec: Vec<SmallRng> =
                seeds.iter().map(|&i| SmallRng::seed_from_u64(i)).collect();
            rng_vec
        }
        // If an RNG is supplied, use it to seed the SmallRNGs
        Some(gen) => {
            let seeds: Vec<u64> = (0..n_seeds).map(|_| gen.gen_range(0, 100_000)).collect();
            let rng_vec: Vec<SmallRng> =
                seeds.iter().map(|&i| SmallRng::seed_from_u64(i)).collect();
            rng_vec
        }
    }
}

fn main() {
    let mut km: KMeans = KMeans::new();
    println!("KM: {:#?}", km);
    let data = vec![
        vec![1.1_f64, 2.2_f64],
        vec![3.3_f64, 5.5_f64],
        vec![0.4_f64, 0.0_f64],
    ];
    km.fit(&data, Some(&mut rand::thread_rng()));
    println!("Inertia: {:?}", km.inertia);
    println!("Labels: {:?}", km.labels);
    println!("Centroids: {:?}", km.cluster_centroids);
    // First and last datapoint should be clustered together
    println!("Predictions: {:?}", km.predict(&data));
}
